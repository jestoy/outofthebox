#!/usr/bin/python3

from bottle import run,route,static_file,template

@route('/')
def index():
    return template('index', title='Home')

@route('/images/<filename>')
def image_folder(filename):
    return static_file(filename, root='images/')

@route('/css/<filename>')
def image_folder(filename):
    return static_file(filename, root='css/')

@route('/license')
def show_license():
    with open('LICENSE.txt', 'r') as myfile:
        data = myfile.read()
    return template('show_license', title="License", data=data)

@route('/admin')
def admin_page():
    return template('admin', title="Administration")

if __name__ == "__main__":
    run(host="localhost", port=9000, reloader=True)
