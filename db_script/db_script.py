import sqlite3 as lite
import hashlib

con = lite.connect('mainDB.db')
cur = con.cursor()

# Create Tables.
cur.execute("""CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, username
    TEXT, password TEXT)""") # Users Table
cur.execute("""CREATE TABLE customers(id INTEGER PRIMARY KEY AUTOINCREMENT,
    firstname TEXT, lastname TEXT, email TEXT, birthday TEXT, username TEXT,
    password TEXT, mobile TEXT, phone TEXT, address TEXT, confirmed TEXT""") # Customer Table
cur.execute()

# Add default details.
username = "admin"
password = "admin"
salt = "L1L1b3Th0$14-3mWan"
password = password + salt
password = hashlib.sha224(password.encode('utf-8')).hexdigest()
cur.execute("""INSERT INTO users VALUES(null,?,?,?)""",(username, password))
con.commit()

# Close Connection
if con:
    cur.close()
    con.close()
